#### 根据注解生成 API 文档
```
# 类的注解  分组,分模块
use joyqhs\RpcSwagger\Annotation\Api;
# 方法的注解 接口描述
use joyqhs\RpcSwagger\Annotation\ApiOperation;
# 参数组的注解
use joyqhs\RpcSwagger\Annotation\ApiParams;
# 参数的注解
use joyqhs\RpcSwagger\Annotation\ApiParam;
# 响应注解
use joyqhs\RpcSwagger\Annotation\ApiResponses;
```
#### 参数说明
| 注解 | 参数名称 | 说明 | 其他 |
| -- | --| --| --|
| Api | group | 分组名称| |
| Api | module | 模块名称| |
| ApiOperation | name | 接口名称| |
| ApiOperation | desc |  接口描述 | |
| ApiParams | name | 参数组名称  | |
| ApiParams | value |  参数组   | 数组,多个ApiParam 或者 ApiParams|
| ApiParam | name | 参数名 | |
| ApiParam | desc | 参数描述 | |
| ApiParam | type | 参数类型 integer string ....|  |
| ApiParam | required | 是否必填 true or false | |
| ApiParam | children | 子参数 ， 一般用于响应结果的多维数组 | 数组 多个 ApiParam |
| ApiResponses | value |  响应组   | 数组,多个ApiParam 或者 ApiParams|

#### 安装
```
	composer require joyqhs/hyperf-rpc-swagger
```
#### 配置
```
php bin/hyperf.php vendor:public joyqhs/hyperf-rpc-swagger
```
在 config/autoload/rpc-swagger.php 中简单配置
#### 使用例子
```
<?php
declare(strict_types=1);

namespace App\JsonRpc\Services\System;

use App\JsonRpc\Interfaces\System\MemberInterface;
use App\Service\User\UserService;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\RpcServer\Annotation\RpcService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\PostMapping;
use joyqhs\RpcSwagger\Annotation\ApiOperation;
use joyqhs\RpcSwagger\Annotation\Api;
use joyqhs\RpcSwagger\Annotation\ApiParams;
use joyqhs\RpcSwagger\Annotation\ApiParam;
use joyqhs\RpcSwagger\Annotation\ApiResponses;


/**
 * @RpcService(name="MemberService", protocol="jsonrpc-http", server="jsonrpc-http",publishTo="consul")
 * @Api(group="系统管理",name="用户管理")
 */

class MemberService implements MemberInterface
{
    /**
     * @Inject
     * @var UserService
     */
    private $service;

    /**
     * @ApiOperation(name="用户信息",desc="用户信息",type="jsonrpc-http")
     * @ApiParams({
     *     @ApiParams(
     *         name="请求头",
     *         type="header",{
     *             @ApiParam(name="authorization",type="string",desc="token")
     *    }),
     * })
     * @ApiResponses({
     *     @ApiResponses(
     *         name="成功响应",
     *         {
     *             @ApiParam(name="status",type="string",desc="状态码"),
     *             @ApiParam(name="result",type="string",desc="数据合集",children={
     *                 @ApiParam(name="username",type="string",desc="用户名"),
     *                 @ApiParam(name="name",type="string",desc="姓名"),
     *                 @ApiParam(name="mobile",type="string",desc="手机"),
     *             })
     *         }
     *     ),
     *     @ApiResponses(
     *         name="失败响应",
     *         {
     *             @ApiParam(name="status",type="string",desc="状态码"),
     *             @ApiParam(name="code",type="string",desc="error"),
     *             @ApiParam(name="error",type="string",desc="错误信息"),
     *             @ApiParam(name="message",type="string",desc="错误信息")
     *         }
     *     ),
     * })
     * @PostMapping (path="member/info")
     */

    public function info()
    {
        $request = make(RequestInterface::class);
        $auth = auth(); // 控制器内也可以通过 @Inject 注入
        $jwtGuard = $auth->guard('jwt');
        $token = $request->getHeaderLine('Authorization');
        $user = $jwtGuard->user($token);
        return $user;
    }

    /**
     * @ApiOperation(name="用户菜单",desc="用户菜单",type="jsonrpc-http")
     * @PostMapping (path="member/menus")
     */

    public function menus()
    {
        $request = make(RequestInterface::class);
        return $this->service->menus($request);
    }
}

```
#### 查看接口
```
 php bin/hyperf.php swagger:rpc:gen 
```
> 可指定参数  -S server.php 中的服务名称 默认 jsonrpc-http
-P 路由地址
-E 导出

#### 命令行
```
 php bin/hyperf.php swagger:rpc:gen   -S admin
 php bin/hyperf.php swagger:rpc:gen   -P auth/login
 php bin/hyperf.php swagger:rpc:gen  -S admin -E 
```
#### 展示

![hyperf 注解文档生成](https://static.dingtalk.com/media/lALPDgfLSVcK4a_NAozNBc8_1487_652.png?auth_bizType=IM&auth_bizEntity=%7B%22cid%22%3A%22349640739%3A349640739%22%2C%22msgId%22%3A%228068385448729%22%7D&bizType=im&open_id=349640739)

![hyperf 注解文档生成](https://static.dingtalk.com/media/lALPDgQ9yrJTwWnM280FIA_1312_219.png?auth_bizType=IM&auth_bizEntity=%7B%22cid%22%3A%22349640739%3A349640739%22%2C%22msgId%22%3A%228050704014152%22%7D&bizType=im&open_id=349640739)

![hyperf 注解文档生成](https://static.dingtalk.com/media/lALPDhYBQ-o0QbbNAo3NBik_1577_653.png?auth_bizType=IM&auth_bizEntity=%7B%22cid%22%3A%22349640739%3A349640739%22%2C%22msgId%22%3A%228050704171311%22%7D&bizType=im&open_id=349640739)

#### 注
目前仅支持导出 easydoc.xyz 的 json  api文件
可以在 easydoc.xyz 中导入 实现模拟请求等
后期会支持更多和内置接口展示。