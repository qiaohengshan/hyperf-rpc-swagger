<?php

namespace joyqhs\RpcSwagger\Annotation;

use Hyperf\Di\Annotation\AbstractAnnotation;

/**
 * @Annotation
 * @Target({"CLASS"})
 */
class Api extends AbstractAnnotation
{
    /**
     * 分组名称
     * @var String
     */
    public $group;

    public $module;

    public function __construct($value = null)
    {
        //$this->bindMainProperty('name', $value);
        parent::__construct($value);
    }

}
