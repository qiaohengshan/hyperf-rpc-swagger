<?php
namespace joyqhs\RpcSwagger\Annotation;

use Hyperf\Di\Annotation\AbstractAnnotation;

/**
 * @Annotation
 * @Target({"ALL"})
 */
class ApiParam extends AbstractAnnotation
{
    /**
     * 参数名
     * @var String
     */
    public $name;

    /**
     * 是否必填
     * @var Boolen
     */
    public $required = true;

    /**
     * 字段类型
     * @var String
     */
    public $type = 'string';

    /**
     * 接口描述
     * @var String
     */
    public $desc = '';

    /**
     * 子参数
     * 一般用于响应
     * @var String
     */
    public $children = [];

    public function __construct($value = null)
    {
        parent::__construct($value);
        $this->bindMainProperty('children', $value);
    }
}
