<?php
declare(strict_types=1);

namespace joyqhs\RpcSwagger;

use Hyperf\Rpc\Contract\PathGeneratorInterface;

class RpcPathGenerator implements PathGeneratorInterface
{
    public function generate(string $service, string $method): string
    {
        $path = str_replace('\\', '/', $service);
        //微服务不需要service，故而将service去掉
        $arr = strtolower(preg_replace('/([a-z])([A-Z])/', "$1" . "_" . "$2", $path));
        list($path, $common) = explode("_", $arr);
        if ($path[0] !== '/') {
            $path = '/' . $path;
        }
        return $path . '/' . $method;
    }
}