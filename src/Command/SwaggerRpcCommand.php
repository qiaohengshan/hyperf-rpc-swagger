<?php

declare(strict_types=1);

namespace joyqhs\RpcSwagger\Command;

use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Command\Annotation\Command;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputOption;
use joyqhs\RpcSwagger\Swagger;

/**
 * @Command
 */
class SwaggerRpcCommand extends HyperfCommand
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container, Swagger $swagger)
    {
        $this->container = $container;
        $this->swagger = $swagger;
        parent::__construct('swagger:rpc:gen');
    }

    public function configure()
    {
        $this->setDescription('Generate rpc documentation through annotations')
            ->addOption('server', 'S', InputOption::VALUE_OPTIONAL, 'Which server you want to describe routes.', 'jsonrpc-http')
            ->addOption('path', 'P', InputOption::VALUE_OPTIONAL, 'Get detailed interface information based on the path')
            ->addOption('export', 'E', InputOption::VALUE_OPTIONAL, 'Get detailed interface information based on the path', false);
    }

    public function handle()
    {
        $server = $this->input->getOption('server');
        $path = $this->input->getOption('path');
        $export = $this->input->getOption('export');
        $apis = $this->swagger->buildDoc($server, $path);
        $rows = [];
        foreach ($apis as $api) {
            $params = [];
            foreach ($api['params'] as $param) {
                $params[] = implode(PHP_EOL, [
                    '<error>' . $param['name'] . '</error>',
                    implode(
                        PHP_EOL,
                        collect($param['datas'])->map(function ($item) {
                            return print_r($item, true);
                        })->toArray()
                    )
                ]);
            }
            $rows[] = [
                $api['group'],
                $api['module'],
                $api['name'],
                $api['method'],
                $api['desc'],
                $api['url'],
                implode(PHP_EOL, $params)
            ];
            $rows[] = new TableSeparator();
        }
        array_pop($rows);

        $table = new Table($this->output);
        $table
            ->setHeaders(['Group', 'Module', 'Title', 'Method', 'Desc', 'Url', 'Params'])
            ->setRows($rows);
        $table->render();

        $this->output->success('success.');
        if ($export === false) {
            return;
        }
        $this->export($apis, $export);
    }

    protected function export($apis, $drive)
    {
        $drives = config('rpc-swagger.drives');
        $list = array_keys($drives);
        if (empty($list)) {
            return;
        }

        if (!array_search($drive, $drives)) {
            $drive = $this->output->choice('export to:', $list, $list[0]);
        }

        $file = (new $drives[$drive])->export($apis);
        if ($file) {
            $this->output->success('export successed. path:' . $file);
        } else {
            $this->output->error('export failed');
        }
    }
}
