<?php
namespace joyqhs\RpcSwagger;

interface ExportInterface{
	public function export($apis);
}
