<?php
namespace joyqhs\RpcSwagger\ExportDrive;

class DefaultExport extends \joyqhs\RpcSwagger\AbstractExport
{
    public function export($apis)
    {
        $docs = [
            'project' => config('rpc-swagger.api_export_options.rpc-easydoc.project'),
            'docs'  => $apis
        ];
        return $this->save($docs);
    }
}
