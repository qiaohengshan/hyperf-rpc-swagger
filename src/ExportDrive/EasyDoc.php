<?php
namespace joyqhs\RpcSwagger\ExportDrive;

class EasyDoc extends \joyqhs\RpcSwagger\AbstractExport{

	public function export($apis){
		$docs = [
			'project' => config('rpc-swagger.api_export_options.rpc-easydoc.project'),
            'docs'  => $this->buildDoc($apis)
		];
		return $this->save($docs);
	}

	protected function buildDoc($apis){
		return collect($apis)->map(function($api){
			$api['sectionDatas'] = collect($api['params'])->map(function($param){
				$param['sid'] = config('rpc-swagger.api_export_options.rpc-easydoc.params_type.'.$param['type']);
				$param['dataType'] = 'params';
				unset($param['type']);
				return $param;
			});
			$api['docType'] = $api['type'];
			unset($api['params'],$api['type']);
			return $api;
		})->toArray();
	}
	
}
