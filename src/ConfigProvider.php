<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace joyqhs\RpcSwagger;

class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'commands' => [
                Command\SwaggerRpcCommand::class
            ],

            'annotations' => [
                'scan' => [
                    'paths' => [
                        __DIR__,
                    ],
                ],

            ],

            'publish' => [
                [
                    'id' => 'SwaggerRpcConfig',
                    'description' => 'swagger rpc config file.', // 描述
                    // 建议默认配置放在 publish 文件夹中，文件命名和组件名称相同
                    'source' => __DIR__ . '/../publish/rpc-swagger.php',  // 对应的配置文件路径
                    'destination' => BASE_PATH . '/config/autoload/rpc-swagger.php', // 复制为这个路径下的该文件
                ],
            ],
        ];
    }
}
