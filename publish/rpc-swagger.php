<?php
return [
    #json导出位置
    'export_path' => BASE_PATH,
    //导出驱动
    'drives'=>[
        'rpc-easydoc' => joyqhs\Swagger\ExportDrive\EasyDoc::class,
        'rpc-joyqhs' =>  joyqhs\Swagger\ExportDrive\DefaultExport::class
    ],
    # 导出驱动相应的配置参数
    'api_export_options' =>[
        'rpc-joyqhs'  => [
            'project' =>[
                'name' => 'joyqhs\Swagger RPC Export Apis'
            ]
        ],
        
        'rpc-easydoc' => [
            'project' => [
                'name' => 'joyqhs\Swagger RPC Export Apis',
                "public" => "0",
                "urlPrefix" => [[
                    "url" => "https://joyqhs.com",
                    "default" => true,
                    "name" => "正式服"
                ],[
                    "url" => "http://localhost:9504",
                    "default" => false,
                    "name" => "本地服"
                ]],
                "desc" => ""
            ],
            'params_type' => [
                'header'    => 1,
                'param'     => 2,
                'response'  => 3,
                'text'      => 4
            ],
        ]
    ],
];